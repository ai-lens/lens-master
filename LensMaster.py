#!/usr/bin/python3
"""
Python launcher for testing and executing this suite of libraries
Copyright (C) 2019  Jack Phoebus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import GraderPython.LensGrader as grader

lens=grader.LensEvaluation()

#lens[1]=5
print(lens)
print("score:",lens.score())

#gradient ascent demo
x=lens.gradientAscent()
print("new score:",lens(x))
print("optimized controls:",lens)#note, lens behaves both like a function and an array