/*Defines interface for pooled threading
Copyright (C) 2019  Jack Phoebus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef THREADPOOL_H
#define THREADPOOL_H
#include <vector>
#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <functional>

class ThreadPool{
	std::vector<std::thread> threads;
	std::queue<std::function<void(void)>> jobs;
	bool shutdown=false;
	
	std::mutex lock;
	std::condition_variable cond;
public:
	ThreadPool(int N);
	~ThreadPool();
	
	void addJob(std::function<void(void)> job);
	void watcher();
	int count();
};

#endif // THREADPOOL_H
