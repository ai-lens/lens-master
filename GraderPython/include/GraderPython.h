/*Defines a python-facing interface for lens-grader
Copyright (C) 2019  Jack Phoebus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "lens.h"
#include <valarray>
#include <Python.h>

struct lensPacking{
	lens l;
	std::vector<controlRef> controls;
	
	std::valarray<double> gradient();
	std::valarray<double> findStart(int n);
};

extern "C"{
	//TODO: add args to this
	lensPacking* createLens();
	void destroyLens(lensPacking* l);
	
	int getControlCount(lensPacking* l);
	double getControl(lensPacking* l,int i);
	void setControl(lensPacking* l,int i,double value);
	
	double evaluateLens(lensPacking* l);
	
	char* geometryJSON(lensPacking* l);
	
	PyObject* gradient(lensPacking* l);
	void doGradientAscent(lensPacking* l);
	
	PyObject* controlList(lensPacking* l);
}