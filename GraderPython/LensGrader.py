#!/usr/bin/python3
"""
Wraps the functionality of lens-grader's api to python
Copyright (C) 2019  Jack Phoebus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import ctypes
import os
import json
from pprint import pprint

#load the grader library
MYPATH=os.path.dirname(os.path.realpath(__file__))
lib=ctypes.cdll.LoadLibrary(os.path.join(MYPATH,"src/libGraderPython.so"))

#establish type signatures
lib.getControlCount.argtypes=[ctypes.c_void_p]
lib.getControlCount.restype=ctypes.c_int

lib.getControl.argtypes=[ctypes.c_void_p,ctypes.c_int]
lib.getControl.restype=ctypes.c_double

lib.setControl.argtypes=[ctypes.c_void_p,ctypes.c_int,ctypes.c_double]
#return is void

lib.evaluateLens.argtypes=[ctypes.c_void_p]
lib.evaluateLens.restype=ctypes.c_double

lib.geometryJSON.argtypes=[ctypes.c_void_p]
lib.geometryJSON.restype=ctypes.c_char_p

lib.gradient.argtypes=[ctypes.c_void_p]
lib.gradient.restype=ctypes.py_object

lib.doGradientAscent.argtypes=[ctypes.c_void_p]
#return is void

lib.controlList.argtypes=[ctypes.c_void_p]
lib.controlList.restype=ctypes.py_object

#wrap with classes
class LensEvaluationIter():
	def __init__(self,lens):
		self.lens=lens
		self.n=len(lens)
		self.i=0
	
	#probably never used, but doing it anyways for now
	def __iter__(self):
		return self
	
	def __next__(self):
		if self.i>=self.n:
			raise StopIteration
		i=self.i
		self.i+=1
		return self.lens[i]

class LensEvaluation():
	def __init__(self):
		self.lens=lib.createLens()
	
	def __len__(self):
		return lib.getControlCount(self.lens)
	
	def __getitem__(self,key):
		return lib.getControl(self.lens,key)
	
	def __setitem__(self,key,value):
		lib.setControl(self.lens,key,value)
	
	def __iter__(self):
		return LensEvaluationIter(self)
	
	def __call__(self,controls=None):
		if controls is not None:
			length=len(controls)
			expected=len(self)
			if length!=expected:
				raise ValueError("Expected input of length {} but received {}".format(expected,length))
			for i in range(length):
				lib.setControl(self.lens,i,controls[i])
		return self.score()
	
	def __str__(self):
		return str(lib.controlList(self.lens))

	def score(self):
		return lib.evaluateLens(self.lens)
	
	def getGeometry(self):
		#python will free this automatically apparently
		charp=lib.geometryJSON(self.lens)
		
		#convert to a python string
		s="".join(map(chr, charp))
		
		#print(s)
		return json.loads(s)
	
	def gradient(self):
		return lib.gradient(self.lens)
	
	def gradientAscent(self):
		lib.doGradientAscent(self.lens)
		return lib.controlList(self)

#testing
if __name__=="__main__":
	#make a sample lens
	lens=LensEvaluation()
	
	#lens[1]=5
	print(lens)
	print("score:",lens.score())
	
	#geometry
	pprint(lens.getGeometry())
	
	#l=[1,.5,6]
	#l=[1,.5,6,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	l=[0.2, 0.1, 0.75, 0.25, 0.2, 0.6, 0.2, 0.75, 0.25, 0.5, 1.0, 0.96, 1.0, 0.45, 1.0, 0.75, 0.7, 1.0, 0.34, 0.1, 1.0]
	print("changing to:",l)
	print("new score:",lens(l))
	print("values now:",lens)
	
	print("gradient:",lens.gradient())