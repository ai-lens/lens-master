/*Implements the python-facing interface for lens-grader
Copyright (C) 2019  Jack Phoebus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "GraderPython.h"
#include "group.h"
#include "element.h"
#include <sstream>
#include <algorithm>
#include <chrono>
#include <iostream>
#include <thread>
#include <random>
#include <tuple>
#include "threadpool.h"
using namespace std;

template<typename Iterator>
static inline void printVector(Iterator begin, Iterator end){
	cout<<"{";
	bool second=false;
	for(; begin != end; ++begin){
		if(second){
			cout<<", ";
		}
		second=true;
		cout << *begin;
	}
	cout<<"}"<<endl;
}

template<typename T>
static inline void applyValuesToControls(lensPacking *l,const T& vals){
	for(int i=0;i<vals.size();i++){
		setControl(l,i,vals[i]);
	}
}
template<typename T>
static inline double applyAndGetScore(lensPacking *l,const T& vals){
	applyValuesToControls(l,vals);
	return l->l.getScore();
}

static ThreadPool pool(thread::hardware_concurrency());
	
valarray<double> lensPacking::gradient(){
	constexpr double dx=0.0000001;
	
	const auto x0=l.getControls();
	const auto N=x0.size();
	
	//printVector(x0.begin(),x0.end());
	
	valarray<double> grad(N);
	const auto fx=l.getScore();
	
	const auto nthread=min<unsigned long>(thread::hardware_concurrency(),N);//if we have more threads than N, no reason to make more than N threads
	volatile bool complete[nthread];
	
	const auto threadfn=[&](const int start,const int stop,const double fx,const int tid){
		for(int i=start;i<stop;i++){
			auto copy=dynamic_pointer_cast<lens>(l.clone());
			auto x=copy->getControlRefs();
			x[i].get()+=dx;
			auto fn=copy->getScore();
			
			grad[i]=(fn-fx)/dx;
		}
		
		complete[tid]=true;
		//printf("job marked done\n");
	};
	
	for(int i=0;i<nthread;i++){
		complete[i]=false;
		pool.addJob(bind(threadfn,(N*i)/nthread,(N*(i+1))/nthread,fx,i));
	}
	
	for(int i=0;i<nthread;i++){
		while(!(complete[i]));
	}
	
	
	//printf("made a grad with %i values\n",grad.size());
	
	return grad;
}


lensPacking* createLens(){
	//for some reason this approach causes a segfault.  is it the move?  investigate
	//lensPacking* l=new lensPacking();
	//l->l=move(lens(vector<double>{.2,.6},100));
	//lensPacking* l=new lensPacking{lens(vector<double>{.2,.6},50),{}};
	lensPacking* l=new lensPacking{lens(1,50),{}};
	
	auto groups=l->l.getGroups();
	vector<shared_ptr<element>> elms;
	elms.reserve(groups.size());
	
	//put one element in each group, store the results in our elms array
	transform(groups.begin(),groups.end(),back_inserter(elms),[](shared_ptr<group> g){
		return g->addElement<element>();
	});
	
	//front element
	auto& elm=elms.back();
	elm->setSphereFront(.75,.7);
	//elm->setSphereBack(.25,0,.5);
	elm->setSphereBack(.4,.1);
	
	groups.back()->setWidth(1);
	//l->l.getGroups()[0]->setMovementMultiplier(0);
	//l->l.getGroups()[1]->setMovementMultiplier(0.5);
	//l->l.getGroups()[2]->setMovementMultiplier(1);
	
	l->controls=l->l.getControlRefs();
	
	/*as a temporary initial measure, restore this state, since process was interrupted
	vector<double> restore={0.113764, 0.981528, 1, 0.0699313, 0.874204, 0.553504, 0.262524, 0.996464, 0.87531, 0.0755885, 0.961295};
	for(int i=0;i<restore.size();i++){
		l->controls[i].get()=restore[i];
	}
	//*/
	
	printf("initial controls: ");
	printVector(l->controls.begin(),l->controls.end());
	
	return l;
}

void destroyLens(lens* l){
	delete l;
}

int getControlCount(lensPacking* l){
	return l->controls.size();
}
double getControl(lensPacking* l, int i){
	//printf("getting index: %i of len %i\n",i,getControlCount(l));
	return l->controls[i];
}
void setControl(lensPacking* l, int i, double value){
	//printf("setting index: %i of len %i\n",i,getControlCount(l));
	l->controls[i].get()=clamp(value,0.,1.);
}

double evaluateLens(lensPacking* l){
	return l->l.getScore();
}

static inline stringstream elmJson(const vector<point>& points){
	stringstream out;
	out<<"[";
	
	//write out each
	bool notFirst=false;
	for(auto& point:points){
		if(notFirst){out<<",";}
		notFirst=true;
		out<<"["<<point.x<<","<<point.y<<"]";
	}
	
	//and done
	out<<"]";
	return out;
}
static inline stringstream groupJson(const groupGeometry& geo){
	stringstream out;
	out<<"{";
	
	//write out the travel distance
	out<<"\"travel\":"<<geo.travel<<",";
	
	//and now the element arrays
	out<<"\"lenses\":[";
	bool notFirst=false;
	for(auto& points:geo.lenses){
		if(notFirst){out<<",";}
		notFirst=true;
		out<<elmJson(points).str();
	}
	out<<"]";
	
	//and done
	out<<"}";
	return out;
}
char* geometryJSON(lensPacking* l){
	auto geo=l->l.getGeometry();
	
	//output json stream
	stringstream out;
	out<<"{";
	
	//important metadata
	out<<"\"sensorToBack\":"<<geo.sensorToBack<<",";
	
	//creat an array for the groups
	out<<"\"groups\":[";
	bool notFirst=false;
	for(auto& group:geo.groups){
		if(notFirst){out<<",";}
		notFirst=true;
		out<<groupJson(group).str();
	}
	out<<"]";
	
	//and done
	out<<"}";
	
	return strdup(out.str().c_str());
}

PyObject* gradient(lensPacking* l){
	auto g=l->gradient();
	PyObject* list=PyList_New(g.size());
	
	for(int i=0;i<g.size();i++){
		PyList_SetItem(list,i,PyFloat_FromDouble(g[i]));
	}
	
	return list;
}

random_device generator;
uniform_real_distribution<double> urand(0.0,1.0);
static inline valarray<double> gradNext(const valarray<double>& x0,const valarray<double>& grad,const double rate){
	return move(x0+rate*grad);
	//return move(x0+rate*urand(generator)*grad);
}
static inline void computeInitial(lens* l,const double searchTime){
	auto bestValue=l->getScore();
	auto bestControls=l->getControls();
	auto refs=l->getControlRefs();
	
	/*
	for(int i=0;i<samples;i++){
		for(int j=0;j<refs.size();j++){
			refs[j].get()=urand(generator);
		}
		auto newscore=l->getScore();
		
		if(newscore>bestValue){
			transform(refs.begin(),refs.end(),bestControls.begin(),[](controlRef c)->double{return c;});
			bestValue=newscore;
		}
	}
	/*/
	const auto nthread=thread::hardware_concurrency();//if we have more threads than N, no reason to make more than N threads
	tuple<double,vector<double>> rvals[nthread];
	volatile bool complete[nthread];
	
	const auto threadjob=[&](const lens* lensOrig,const double searchTime,const int tid){
		auto l=dynamic_pointer_cast<lens>(lensOrig->clone());
		
		auto bestValue=l->getScore();
		auto bestControls=l->getControls();
		auto refs=l->getControlRefs();
		
		auto start=chrono::high_resolution_clock::now(),now=start;
		unsigned long iter=0;
		
		while(chrono::duration<double>(now-start).count()<searchTime){
			for(int j=0;j<refs.size();j++){
				refs[j].get()=urand(generator);
			}
			auto newscore=l->getScore();
			
			if(newscore>bestValue){
				transform(refs.begin(),refs.end(),bestControls.begin(),[](controlRef c)->double{return c;});
				bestValue=newscore;
			}
			
			now=chrono::high_resolution_clock::now();
			++iter;
		}
		
		rvals[tid]=tie(bestValue,bestControls);
		complete[tid]=true;
		
		printf("From computeInitial: Thread %i completed %lu iterations in %f seconds\n",tid,iter,chrono::duration<double>(now-start).count());
	};
	
	for(int i=0;i<nthread;i++){
		complete[i]=false;
		pool.addJob(bind(threadjob,l,searchTime,i));
	}
	
	for(int i=0;i<nthread;i++){
		//TODO: condition var instead of bool?
		while(!complete[i]);
		
		auto newval=get<0>(rvals[i]);
		if(newval>bestValue){
			bestValue=newval;
			bestControls=get<1>(rvals[i]);
		}
	}
	//*/
	
	//now make the lens be in the optimal state
	for(int i=0;i<bestControls.size();i++){
		refs[i].get()=bestControls[i];
	}
}
void doGradientAscent(lensPacking* l){
	constexpr double
		minDelta=1e-32,
		minRate=1e-40;
	double rate=1e-6;
	double delay=3,designDelay=30;//output delays in seconds
	long totalIterations=0;
	
	//get ourselves into a good intiial state
	printf("Finding a good initial state\n");
	computeInitial(&l->l,4*60);
	
	//debugging, print initial state
	auto initialControls=l->l.getControls();
	printf("Using initial state: ");
	printVector(initialControls.begin(),initialControls.end());
	
	//we'll be using valarr for it's various math features
	valarray<double> x0(l->controls.size());
	transform(l->controls.begin(),l->controls.end(),begin(x0),[](auto& x){return (double)x;});
	
	double fx0=l->l.getScore();
	printf("Initial score: %f\n",fx0);
	
	auto start=chrono::high_resolution_clock::now(),designStart=start;
	
	while(true){
		const auto grad=l->gradient();
		valarray<double> x1=gradNext(x0,grad,rate);
		
		auto fx1=applyAndGetScore(l,x1);
		
		//*
		while(fx1<fx0){
			//we dont want the value of f to drop, so keep cutting the rate in half and try again
			//TODO: alternative break condition to prevent infinite loop?
			if(rate>minRate){
				rate*=0.5;
			}
			x1=gradNext(x0,grad,rate);
			fx1=applyAndGetScore(l,x1);
		}
		//*/
		
		if(rate<=minRate){
			printf("exit from rate\n");
			//return x0;
			return;
		}
		
		if(abs(fx1-fx0)<minDelta){
			printf("exit from delta\n");
			//return x1;
			return;
		}
		
		
		auto end=chrono::high_resolution_clock::now();
		chrono::duration<double> elapsed=end-start;
		if(elapsed.count()>delay){
			printf("current value: %.*f\tTotal steps:%li\n",16,fx1,totalIterations);
			start=end;
		}
		elapsed=end-designStart;
		if(elapsed.count()>designDelay){
			printVector(l->controls.begin(),l->controls.end());
			designStart=end;
		}
		
		x0=move(x1);
		fx0=fx1;
		++totalIterations;
	}
}

PyObject* controlList(lensPacking* l){
	const auto N=l->controls.size();
	PyObject* list=PyList_New(N);
	
	for(int i=0;i<N;i++){
		PyList_SetItem(list,i,PyFloat_FromDouble(l->controls[i]));
	}
	
	return list;
}
