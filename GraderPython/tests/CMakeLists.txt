project(GraderPython-GradientTest)
find_package(PythonLibs REQUIRED)

add_executable(${PROJECT_NAME} gradient.cpp)
target_link_libraries(${PROJECT_NAME} GraderPython)

#include PythonLibs
include_directories(${PYTHON_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} ${PYTHON_LIBRARIES})