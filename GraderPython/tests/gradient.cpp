/*Tests gradient descent functionality
Copyright (C) 2019  Jack Phoebus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "GraderPython.h"
#include <iostream>
using namespace std;

template<typename Iterator>
static inline void printVector(Iterator begin, Iterator end){
	cout<<"{";
	bool second=false;
	for(; begin != end; ++begin){
		if(second){
			cout<<", ";
		}
		second=true;
		cout << *begin;
	}
	cout<<"}"<<endl;
}


int main(int argc,char** argv){
	lensPacking* lens=createLens();
	
	printf("score: %f\n",evaluateLens(lens));
	
	//gradient ascent demo
	doGradientAscent(lens);
	auto x=evaluateLens(lens);
	printf("new score: %f\n",x);
	printf("new design: ");
	printVector(lens->controls.begin(),lens->controls.end());
	char* json=geometryJSON(lens);
	cout<<json<<endl;
	delete[] json;
	return 0;
}